---
title: "Advanced FAQ"
weight: 2
---

Here you can find some advanced questions for/from end-users.

### How sustainable is XMPP/Jabber? {#faq-advanced-sustainability}

Will my account still work 10 years from now?

XMPP/Jabber was standardized in the early 2000's (20 years ago) and some hosted services are just a few years younger. They will probably still operate a decade from now. Other servers are operated by non-profits with a sustainable model: associations promoting free-software and privacy (like [April](https://chapril.org/), [5July](https://5july.org/) and [La Quadrature](https://www.laquadrature.net/en/)), non-profit Internet Service Providers (like [franciliens.net](https://www.franciliens.net/) and [ARN](https://arn-fai.net/)), associations dedicated to Jabber/XMPP (like [jabber.fr](https://jabber.fr/) and TODO many others), or hosting cooperatives (like [disroot](https://disroot.org/)). These servers have good chances to still operate ten years from now.

Other servers run by corporations, universities and enthusiasts may still operate in a decade because hosting Jabber/XMPP services requires very little resources. However, there's no way to know for sure because their economic model and incentives may not be aligned with their users’. However, it is common in the Jabber/XMPP ecosystem for an operator ceasing activities to warn their users a long time in advance (as there is no business incentive not to do so), so they have time to migrate to another server.

Hosting Jabber/XMPP services is not complicated for systems administrators, so the most future-proof option may be running your own server. If you are a member of a non-profit association, a cooperative, or another collective entity with a dedicated sysadmin team, consider running your own server. If your services are already handled by a third-party hosting cooperative, and you have good faith that they will continue to operate for a long time, you may ask them to host a Jabber/XMPP service for you, in addition to the usual Web/email/DNS offering.

### Jabber/XMPP doesn't have a certain feature, why is it moving so slow? {#faq-advanced-evolution}

XMPP is designed to be easily extendable and has evolved into a modern ecosystem for real-time messaging in recent years. If you have bad memories from a decade ago, you should give it a try again. Some older clients don't have all the features one would expect from a modern messenger, but those advertised on our homepage should be satisfactory. Please let us know if that is not the case.

Also, XMPP being a standard protocol, all proposals are reviewed and evolved publicly, mostly on mailing lists and on the [XMPP Standards Foundation](https://xmpp.org/) website. This may sound like it prevents innovation, but in fact it is quite the contrary. The specification process, however cumbersome it is, does not prevent developers from extending existing features or implementing new ones because a new specification is not approved (yet). It merely helps other developers get up to speed if and when they want to.

### How does XMPP compare to Matrix in more detail? {#faq-advanced-matrix}

_This is a somewhat subjective comparison (feel free to disagree)._

**Jabber/XMPP:**
- well-established and stable
- many clients on most platforms, some with a modern set of features, others less so
- a dedicated, primarily non-profit, ecosystem, both for clients and servers
- scalable to many users on low resources server-side (think *Raspberry Pi*)
- works well with Tor and other privacy-friendly proxies
- chat rooms and servers minimize meta-data sharing, making it more privacy-preserving especially if you run your own XMPP server
- while bridging to other IM systems is available, efforts have been scaled back in recent years
- has accumulated some technical debt over the years

**Matrix:**
- Element (the official web-based Matrix client), has a modern interface and user experience
- chatrooms are censorship-resistant, because they do not live on a single server
- dedicated funding/resources to advance the ecosystem more quickly
- primarily developed by a for-profit UK based company
- a server-resource heavy reference implementation (Synapse), making it difficult/costly to self-host
- a relatively centralized federated network, with (at least by default) centralized identity services
- a commitment to [peer-to-peer use cases](https://matrix.org/blog/2020/06/02/introducing-p-2-p-matrix/) (less advanced [in the XMPP ecosystem](https://xmpp.org/extensions/xep-0174.html))
- emphasis on bridging to other IM systems with gateways created by the main developers
- has a still quite new and in large parts experimental code-base

As you can see, both protocols have their strengths and weaknesses, but they still operate on similar principles. In theory, it would have been possible (and easier) to build the matrix protocol on top of XMPP to avoid [fragmenting the ecosystem](https://xkcd.com/927/). Why this was not done is a good question, but the fact is we now have two protocols/ecosystems with similar goals, and we should try to dismantle our egos and work for interoperability, for the benefit of end-users.
