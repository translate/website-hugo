---
title: "FAQ voor beheerders"
weight: 5
---

## Vragen over serverbeheer (zelf gehost of niet) {#faq-op}

### Kan ik een Jabber/XMPP-dienst draaien of huren onder mijn eigen domein(en)? {#faq-op-domain}

Ja, een enkele Jabber/XMPP-server kan gemakkelijk meerdere domeinen bedienen (virtuele hosts) en er zijn projecten die aliasdiensten aanbieden, vergelijkbaar met hoe sommige e-mailproviders dat doen. We adverteren mogelijk dergelijke dienstverleners in de sectie [*Collectief*]({{< ref "docs/servers/collective" >}}) van onze website.

### Heb ik een dure server nodig om zelf een XMPP/Jabber dienst te draaien? {#faq-op-resources}

Nee, Jabber/XMPP servers zijn extreem efficiënt. De eenvoudigste VPS van een hostingprovider of een goedkope ARM single-board computer zoals de Rasberry Pi kan gemakkelijk honderden, zo niet duizenden gebruikers tegelijkertijd bedienen.

### Kan ik een Jabber/XMPP-server draaien voor mijn goede vrienden? {#faq-op-friends}

Ja, in feite is dat een veel voorkomende gebruikssituatie. Dankzij de federatieve aard van het netwerk kun je nog steeds met iedereen communiceren die Jabber/XMPP gebruikt en kun je minder technisch onderlegde vrienden en familie laten genieten van een echt privacybehoudend instant messagingsysteem.

Als je op zoek bent naar een complete Jabber/XMPP-distributie (client en server) om een kleine server voor vertrouwde mensen te hosten, kijk dan eens naar het project [Snikket](https://snikket.org/). Snikket-servers bieden out-of-the-box de mogelijkheid voor bestaande gebruikers om [nieuwe gebruikers uit te nodigen](https://blog.prosody.im/great-invitations/) om lid te worden van je server, zonder registraties open te stellen voor de hele planeet.

### Kan ik een Jabber/XMPP-server draaien op een gedeelde PHP-website hosting? {#faq-op-php}

Sorry, dit is over het algemeen niet mogelijk, omdat Jabber/XMPP-servers continu moeten draaien en dit wordt verhinderd door de meeste shared hostingproviders (die verwachten dat PHP apps alleen worden gebruikt voor het serveren van websites). Dit is deels de reden waarom er geen moderne XMPP-servers geschreven zijn in PHP, ook al is het in theorie mogelijk om er een in PHP te schrijven.

### Moet ik de registraties van mijn server openstellen voor het publiek? {#faq-op-public}

Het hosten van internetdiensten komt met enige verantwoordelijkheid en wettelijke verplichtingen. Je zult zorgvuldig moeten overwegen of je de server wilt openstellen voor mensen die niet zijn door te lichten en misschien bepaalde principes niet met je delen. Open registraties (vooral "in-band-registration") kunnen worden misbruikt door spammers. Als je server door spammers wordt misbruikt, is het mogelijk dat andere servers de communicatie met je server blokkeren.

Echter, gezien de federatieve aard van Jabber/XMPP, is er niets mis mee om te voorkomen dat volslagen vreemden accounts op jouw server aanmaken zonder jouw medeweten/goedkeuring, omdat ze dan elders een account kunnen aanmaken en met jouw gebruikers kunnen communiceren. Als middenweg kan je overwegen om gebaseerd op [uitnodigingen](https://blog.prosody.im/great-invitations/) registraties in te stellen. Voor chats met meerdere gebruikers kun je overwegen om anonieme logins in te schakelen zodat gebruikers die (nog) geen account hebben toch aan het gesprek kunnen deelnemen.

Tenslotte, een open server beheren is echt cool. Als je dat zou willen doen, bekijk dan hier enkele tips over [spam management](https://blog.prosody.im/simple-anti-spam-tips/).

### Kan ik mijn server federeren met onion-servers? {#faq-op-onions}

Ja, het is mogelijk om een XMPP-server te configureren als een service over [Tor](https://www.torproject.org/). We bieden hier een [korte handleiding]({{< ref "tutorials/service/tor" >}}) aan.

### Wat zijn de wettelijke vereisten voor het hosten van een Jabber/XMPP-dienst? {#faq-op-legal}

Of je nu gebruikers uit de Europese Unie bedient of niet, je zult op de hoogte moeten zijn van de gegevensbeschermingsvereisten van de [AVG](https://nl.wikipedia.org/wiki/Algemene_verordening_gegevensbescherming), omdat deze goed advies levert voor elk soort dienst. Afhankelijk van waar de server is gehost en het land waar de systeembeheerder woont, kunnen meer specifieke voorschriften van toepassing zijn. Wij geven momenteel geen specifieker advies in deze FAQ, maar bijdragen zijn welkom.

### Welke andere zorgen moet ik hebben? {#faq-op-concerns}

In tegenstelling tot op winst gerichte diensten, geloven wij dat het draaien van een XMPP-server bij voorkeur geen doel op zich moet zijn. Diensten/gereedschappen zijn vaak duurzamer als ze een specifieke behoefte of gemeenschap dienen in plaats van een ongespecificeerd algemeen publiek. Bedenk ook dat het altijd beter is om meer dan één beheerder van de dienst te hebben, zodat in het geval er iets met jou zou gebeuren, iemand anders het gemakkelijk kan overnemen.
