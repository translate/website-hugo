---
title: "FAQ voor gateways"
weight: 4
---

Jabber/XMPP gateways & bridges met andere netwerken.

### Terminologie

- "**Gateways**" of "**transports**" stellen gebruikers in staat verbinding te maken met andere netwerken (IRC, Matrix...) alsof ze er rechtstreeks op zijn aangesloten; de gateway creëert een gebruikerssessie op het externe netwerk in een proces dat "**puppeteering**" wordt genoemd;
- "**Bridges**" verbinden collectieve ruimten (zoals chatrooms) over netwerken heen zonder tussenkomst van de gebruiker; zij kunnen berichten doorgeven via een specifieke gebruiker (relay bot) of door "**puppeteering**" van gebruikers op externe netwerken. Bij sommige netwerken op afstand kan de relay bot ook tijdelijk zijn naam veranderen, wat "**user spoofing**" wordt genoemd.

De termen gateway en bridge worden soms door elkaar gebruikt. Op het Matrix-netwerk worden beide bijvoorbeeld "bridges" genoemd, maar er wordt onderscheid gemaakt tussen "portaled" (=gatewayed) en "plumbed" (= bridged) rooms.

### Beperkingen

- Doordat dergelijke systemen vaak reverse-engineered zijn, zijn ze vatbaar voor fouten en vaak in strijd met de servicevoorwaarden, vooral op commerciële netwerken zoals Discord;
- Door de problemen met het matchen van verschillende protocollen zijn de gebruiksmogelijkheden aan beide kanten bijna altijd verminderd;
- Sommige netwerken verbieden openbaar toegankelijke gateways, omdat ze gemakkelijk door spammers kunnen worden misbruikt en technisch moeilijk te onderscheiden zijn van geautomatiseerde spam-bots.

### Privacy en veiligheid 

- Doordat de berichten van de gebruikers aan externe netwerken worden blootgesteld, wordt de privacy van de gebruikers doorgaans niet gewaarborgd en is het zelden mogelijk om de toestemming van alle gebruikers te verkrijgen;
- Kwaadwillende gateways en bridges kunnen de inhoud van berichten die zij doorgeven controleren, aangezien netwerkcodering (TLS) aan beide zijden op de gateway zelf wordt beëindigd;
- Credentials (zoals wachtwoorden) die door gateways voor inlogdoeleinden worden gebruikt, worden soms op de gatewayserver opgeslagen; met name voor wachtwoordloze systemen zoals Telegram kan er ook een risico van permanente accountovername bestaan bij het gebruik van openbare gateways. [2FA](https://en.wikipedia.org/wiki/Multi-factor_authentication) dient in dergelijke gevallen te worden gebruikt;
- Sommige gateways stellen de JID van de gebruiker publiekelijk bloot aan andere netwerken, soms in het openbaar, in tegenstelling tot hoe de meeste XMPP-diensten werken (beheerders van een XMPP-groepschat kunnen altijd de JID van een gebruiker zien, maar gewone gebruikers meestal niet); deze informatie wordt door de gateway blootgesteld aan externe netwerken ter voorkoming van misbruik (om individuele gebruikers te kunnen onderscheiden);
- User-spoofing bridges kunnen gemakkelijk misbruikt worden om zich als gebruiker voor te doen.

### Gateway / bridge etiquette

- Als je de gast bent moet je je aan de regels van de gastheer houden. Dus als je bijvoorbeeld een XMPP -> IRC gateway gebruikt, moet je de afspraken van IRC respecteren. Maar als je een IRC -> XMPP gateway gebruikt, moet je de regels van XMPP respecteren;
- Vermijd het gebruik van functies die gewoonlijk niet (reacties, geneste reacties enz.) of slechts in zeer beperkte mate (bijvoorbeeld citeren) over gateways en bruggen worden vertaald;
- Vraag voordat je een bridge of gateway opzet beleefd aan de beheerder van de chatroom of de eigenaar van de dienst of dit is toegestaan (maar in het geval van commerciële netwerken kan dat onmogelijk zijn);
- In de meeste gevallen werken gateways en bridges alleen betrouwbaar als jij de beheerder bent van beide netwerken.
