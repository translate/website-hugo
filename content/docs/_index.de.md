---
title: "Los geht's"
weight: 1
---

Einfach in zwei Schritten:

1. Melde dich auf einem Server an
2. Suche dir eine App aus

<!-- Find a way to make a server selection wizard for easy on-boarding? -->

{{< button "servers/" "Finde einen Server" "mb-1" >}}

{{< button "apps/" "Wähle eine App aus" >}}

Glückwunsch! Jetzt kannst du due von dir gewählte App öffnen, deine Zugangsdaten eingeben, und dann anfangen mit deinen Freunden zu chatten.

Noch Niemanden zum chatten? Du kannst auch bei unserem Gruppenchat vorbeischauen: {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}}.

Wir haben auch einen beginnerfreundlichen Supportchat: {{< chatlink "support" >}}support@joinjabber.org (web chat){{< /chatlink >}}. Der Webchat funktioniert ganz ohne Zugangsdaten!

Weitere Fragen? Antworten gibt es hier:
{{< button "faqs/" "FAQ" "mb-1" >}}
