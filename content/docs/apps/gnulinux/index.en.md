---
title: "GNU/Linux"
---

## [<img src="/images/apps/dino.svg" style="max-height:30px;height:100%"> Dino](#dino-linux) {#dino-linux}

[Dino](https://dino.im/) is a new modern looking Jabber/XMPP app for GNU/Linux. It supports audio & video chat. Get it from your distribution's package manager.

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-linux) {#gajim-linux}

[Gajim](https://gajim.org/) is a fully featured Jabber/XMPP app that runs on GNU/Linux. Get it from your distribution's package manager or here:

[<img alt="Get it on Flathub" src="/images/apps/flathub.svg" style="max-height:65px;height:100%">](https://flathub.org/apps/details/org.gajim.Gajim)

