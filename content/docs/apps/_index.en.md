---
title: "Apps"
description: "Apps for your operating systems"
weight: 3
---

Because Jabber is an open network, there are many client apps to choose from. But to make your choice a bit easier, we have tried to select the best apps for you.

First you need decide where to install the app:

### For mobile phones and tablets:

{{< button "android/" "Android" "mb-1" >}}

{{< button "ios/" "iOS" >}}

_For other mobile platforms we sadly do not have any good recommendations right now._

### For desktop and laptop computers:

{{< button "gnulinux/" "GNU/Linux" "mb-1" >}}

{{< button "macos/" "Mac OS" >}}

{{< button "windows/" "Windows" >}}

_There are also platform independent web clients such as [Movim](https://movim.eu)._
