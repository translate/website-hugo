---
title: "Android"
---

## [<img src="/images/apps/conversations.svg" style="max-height:30px;height:100%"> Conversations](#conversations-android) {#conversations-android}

De door ons aanbevolen client is [Conversations](https://conversations.im). U kunt Conversations downloaden via [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations/), de open-source app store voor Android. Als je F-Droid nog niet geïnstalleerd hebt, raden we je sterk aan om hiervoor de tijd te nemen. Als je de ontwikkeling van Conversations wilt steunen, kun je de app ook kopen via de Play Store van Google, of [doneren](https://conversations.im/#donate) aan het project.

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/eu.siacs.conversations/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=eu.siacs.conversations)

## [<img src="/images/apps/cheogram.svg" style="max-height:30px;height:100%"> Cheogram](#cheogram-android) {#cheogram-android}

[Cheogram](https://cheogram.com/) is een fork van Conversations met wat extra functionaliteit.

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/com.cheogram.android/)

## [<img src="/images/apps/blabber.png" style="max-height:30px;height:100%"> Blabber.im](#blabber-android) {#blabber-android}

[Blabber.im](https://blabber.im/en.html) is nog een fork van Conversations met als doel extra gebruiksgemak te bieden. Installeer het [via hun website](https://blabber.im/en.html) of F-Droid.

_Als je geen F-Droid gebruikt, of als je wilt gebruikmaken van Google's servers voor pushmeldingen, installeer dan de [Google Play APK](https://blabber.im/download-ps.php) in plaats van een store te gebruiken. Het zal automatisch zoeken naar updates zodra het is geïnstalleerd, zodat je geen beveiligingsfixes of nieuwe functionaliteit zult missen._

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/packages/de.pixart.messenger/)


