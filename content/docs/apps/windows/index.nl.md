---
title: "Windows"
---

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-windows) {#gajim-windows}

[Gajim](https://gajim.org/) is een volledig uitgeruste Jabber/XMPP-applicatie die draait op o.a. Windows. Installeer het via [hun website](https://gajim.org/download/).
