---
title: "iOS"
---

## [<img src="/images/apps/monal.svg" style="max-height:30px;height:100%"> Monal](#monal-ios) {#monal-ios}

[Monal](https://monal-im.org/) is a modern Jabber/XMPP client for iOS and Mac OS. Get it here:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/app/id317711500)

## [<img src="/images/apps/siskin.png" style="max-height:30px;height:100%"> Siskin](#siskin-ios) {#siskin-ios}

[Siskin](https://siskin.im/) is another Jabber/XMPP client for iOS. Get it here:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/app/us-app-tigase-messenger/id1153516838)

