---
title: "Zelf gehost"
description: "Servers voor zelfhosting"
weight: 4
---

Het is ook mogelijk je eigen server te draaien. Hier vind je enkele opties om dit te doen. Meer details zijn te vinden in onze [handleidingen]({{< ref "tutorials" >}}).

Homebrew Server club is ook een goede plek om [meer te weten te komen](https://homebrewserver.club/category/instant-messaging.html).

Als u van plan bent anderen toe te staan uw XMPP-server te gebruiken, overweeg dan de aansluiting bij ons [Convenant voor Server]({{< ref "about/community/covenant" >}}) lees onze [handleiding voor het draaien van een (semi-)openbare service]({{< ref "tutorials/service/public" >}}).

Wij raden het gebruik van [Prosody](https://prosody.im/doc) or [Ejabberd](https://docs.ejabberd.im/) aan voor het aanbieden van je eigen XMPP-server. Maar de volgende opties maken het wat makkelijker om te beginnen:

## Snikket

[<img alt="Get started" src="/images/servers/snikket.svg" style="max-height:60px;height:100%">](https://snikket.org/service/quickstart/)

Snikket is een project gestart door een van de [Prosody](https://prosody.im)-ontwikkelaars met als doel een uniforme ervaring en een [gebruiksvriendelijke serverhosting](https://snikket.org/hosting/) te bieden. Je kunt meer informatie vinden op [hun website](https://snikket.org/about/goals/).

## Yunohost

[<img alt="Get started" src="/images/servers/yunohost.svg" style="max-height:100px;height:100%">](https://yunohost.org)

YunoHost is een besturingssysteem gericht op eenvoudig beheer van een server en om zelf-hosting te democratiseren. Het wordt geleverd met een [voorgeïnstalleerde XMPP-server](https://yunohost.org/en/XMPP) naast andere eenvoudig te installeren software voor je server.

## Uberspace

[<img alt="Get started" src="/images/servers/uberspace.svg" style="max-height:100px;height:100%">](https://uberspace.de/en/)

Als je de voorkeur geeft aan iets tussen een beheerde dienst en het draaien van je eigen server of VPS in, kun je terecht bij Uberspace. Zij bieden een [gedetailleerde handleiding hoe je je eigen Prosody XMPP-dienst](https://lab.uberspace.de/guide_prosody/) op hun gedeelde server kunt draaien.

**Meer servers?**

Dit is uiteraard geen volledige lijst! Als je een echt goede aanbeveling hebt, neem dan [contact met ons op](xmpp:chat@joinjabber.org?join). 
