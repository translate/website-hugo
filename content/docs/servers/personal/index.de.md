---
title: "Persönlich"
description: "Individueller Zugang"
weight: 1
---

Warscheinlich suchst du nach einem nicht kommerziellen Anbieter der ein nachhaltiges Betriebskonzept hat, so dass dieser mit hoher Wahrscheinlichkeit auch in 20 Jahren noch besteht. Oder brauchst du einen Server mit anonymen Zugang über das Tor Netzwerk? Hier ein paar Empfehlungen:

## Quicksy

Quicksy ist ein benutzerfreundliches Angebot für Android Handys vom Entwickler der [Conversations App](/docs/apps/android/#conversations-android), welches es ermöglicht einfach einen Jabber Zugang basierend auf deiner Mobiltelefonnummer zu erstellen. Andere Quicksy Nutzer in deinem Telefonbuch werden automatisch von der App erkannt. Genaueres kannst du auf der [Quicksy Website](https://quicksy.im/) erfahren. Anmelden geht ganz einfach innerhalb der App:

[<img alt="Von F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/im.quicksy.client)
[<img alt="Von Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=im.quicksy.client)

## Chalec

[<img alt="Erstelle einen Zugang" src="/images/servers/chalec.png" style="max-height:100px;height:100%">](https://www.chalec.org/services/xmpp.html)

[Chalec.org](https://www.chalec.org/services/xmpp.html) ist ein nichtkommerzieller Anbieter aus Frankreich. Du kannst direkt aus einer [Jabber App](/docs/apps/) dich anmelden.

## XMPP.social

[<img alt="Erstelle einen Zugang" src="/images/servers/hookipa.png" style="max-height:60px;height:100%">](https://hookipa.net/register/new)

XMPP.social ist eine von mehreren Jabberadressen die du kostenlos auf [Hookipa.net](https://hookipa.net/) erstellen kannst. Dieser Server befindet sich in Deutschland.

## Disroot

[<img alt="Erstelle einen Zugang" src="/images/servers/disroot.svg" style="max-height:40px;height:100%">](https://user.disroot.org/pwm/public/newuser)

Disroot ist ein nichtkommerzielles Projekt aus den Niederlanden das in 2015 gegründet wurde. Es besteht aus einer Gruppe von Freiwilligen die nicht nur Jabber sonder auch noch andere Onlinedienste anbieten. Du kannst mehr über sie auf [ihrer Website](https://disroot.org/en/about) erfahren.

## XMPP.is

[<img alt="Erstelle einen Zugang" src="/images/servers/xmppis.png" style="max-height:60px;height:100%">](https://xmpp.is/account/register/)

[XMPP.is](https://xmpp.is/) ist ein auf Privatsphäre fokussierter Anbieter mit Servern in Island. Sie bieten einen [Zugang über das Tor Netzwerk an](https://xmpp.is/2021/11/10/tor-hsv3-registrations-are-now-open/).

**Andere?**

Natürlich ist dies keine komplette Liste und wenn du weiter gute Empfehlungen hast einfach mal in unserem chat: {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}} vorbei schauen. 

Fallst du einer der Betreiber der oben genannten Server bist und nicht hier gelistet werden möchtest trete bitte mit uns in Kontakt: {{< chatlink "servers" >}}servers@joinjabber.org (web chat){{< /chatlink >}}.

