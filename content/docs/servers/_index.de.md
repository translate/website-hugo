---
title: "Servers"
description: "Empfohlende servers"
weight: 2
---

{{< tip >}}
Jabber ist [föderiert](docs/faqs/user/#faq-users-federation), so ähnlich wie E-mail oder Mastodon. Viele Server sind miteinander verbunden und formen somit das Jabbernetzwerk.

Du kannst deinen Server frei wählen und mit deinen Freunden chatten, egal mit welchem Server sie verbunden sind.
{{< /tip >}}

{{< tip "warning" >}}
Jabberadressen (abgekürzt zu JID) schauen genau wie E-mail Adressen aus. Erstellst du zum Beispiel einen Zugang wie `emma.goldman` auf dem Server `example.org`, so wäre deine Adresse `emma.goldman@example.org`.

Gruppenchats haben eine ähnliche Adresse wie zum Beispiel `chat@groups.example.org`.
{{< /tip >}}

Als Erstes musst du überlegen welche der folgenden Kategorien am besten beschreiben welche Art von Jabberserver du benötigst.

### Ich hätte gerne einen:

{{< button "personal/" "Persönlichen Zugang" >}}

_Diese Kategorie ist für Individuen die einen verlässlichen Jabberserver haben wollen._

### Ich habe interesse an einem Server für mein:

{{< button "collective/" "Kollektiv" >}}

_Diese Kategorie ist für Gruppen und Organisationen die mehrere Jabberaddressen und eine professionelle Dienstleistung benötigen._

### Ich hätte gerne einen:

{{< button "selfhosted/" "Eigenen Server" >}}

_Diese Kategorie ist für abenteuerlustige Leute die gerne etwas selber machen._

### Warum gibt es so viel Auswahl?

Das Jabbernetzwerk ist eine weitreichendes und dezentralisiertes Projekt an dem viele Individuen und Organisationen teilnehmen. Es gibt keine zentrale Firma die alles bestimmt und die Protokollentwicklung ist gemeinschaftlich durch die [XMPP Standards Foundation](https://xmpp.org/about/xmpp-standards-foundation/) organisiert. Dies bedeutet, dass es viele Server zur Auswahl gibt, was vielleicht Anfangs etwas verwirrend ist. Es gibt verschiede Versuche ([1](https://xmpp.org/software/servers/), [2](https://providers.xmpp.net/), [3](https://list.jabber.at/)) all diese Server mit unterschiedlichem Erfolg aufzulisten, aber hier auf [JoinJabber.org](/) haben wir versucht ein paar gute Empfehlungen für dich herauszusuchen.

### Serverumzug

Du hast dich umentschieden oder einen Fehler bei der ursprünglichen Serverwahl gemacht? Kein großes Problem! Einfacher Serverumzug ist zwar noch im [Aufbau](https://docs.modernxmpp.org/projects/portability/) für große Teile des Jabbernetzwerkes, aber es gibt [hier](https://migrate.modernxmpp.org/) eine Webseite die den Umzug erleichtert. Da diese Webseite deine Zugangsdaten erfordert, ist es empfehlenswert die Passworter nach Benutzung zu ändern.

### Kann ich nicht einfach einen Zugang hier bekommen?

Nein, wir von JoinJabber bieten keine Jabberzugänge an. Genaueres hierzu kannst du in den Zielen unseres Projektes [hier](/about) finden.
