---
title: "Over JoinJabber"
---

JoinJabber is een informeel internationaal collectief van individuen met diverse achtergronden, dat als gemeenschappelijk doel heeft het gebruik van Jabber/XMPP, een vrij en gefedereerd sociaal netwerk met een focus op real-time chat-toepassingen, te promoten.

Als je geïnteresseerd bent in onze doelen, lees dan [hier]({{< ref "/about/goals" >}}) verder.
