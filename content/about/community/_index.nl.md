---
title: "Gemeenschap"
weight: 3
---

Wij zijn een nieuw collectief dat de gebruikerservaring in het Jabber/XMPP-ecosysteem wil helpen verbeteren. Je kunt op deze website meer lezen over [onze doelen](about/goals/), of de [notulen van onze vergaderingen](about/history/) nalezen. We hebben geen enkele vorm van formeel lidmaatschap, dus als je vindt dat we gemeenschappelijke interesses hebben, dan ben je van harte welkom!

Wil je met ons in contact komen en/of bijdragen aan het project? Dan hebben we verschillende chatrooms waar je terechtkunt. Op dit moment zijn al deze chatrooms in het Engels, maar we verwelkomen initiatieven om nieuwe chatrooms voor andere talen te creëren:

- {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}}: een algemene chatroom voor het collectief, om vragen te stellen en betrokken te raken
- {{< chatlink "support" >}}support@joinjabber.org (web chat){{< /chatlink >}}: algemene ondersteuning voor Jabber/XMPP-gebruikers
- {{< chatlink "servers" >}}servers@joinjabber.org (web chat){{< /chatlink >}}: technische ondersteuning voor Jabber/XMPP-serverbeheer
- {{< chatlink "project" >}}project@joinjabber.org (web chat){{< /chatlink >}}: plaats voor discussies door/met de werkgroepen van JoinJabber.
- {{< chatlink "abuse" >}}abuse@joinjabber.org (web chat){{< /chatlink >}}: misbruik en moderatie in de Jabber/XMPP-federatie
- {{< chatlink "privacy" >}}privacy@joinjabber.org (web chat){{< /chatlink >}}: discussies over privacy en veiligheid, voor gebruikers en serverbeheerders
