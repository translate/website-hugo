---
title: "Community"
weight: 4
---

We are a new collective who would like to help improve user experience in the Jabber/XMPP ecosystem. You can read more about [our goals](/about/goals/), or read our last meeting minutes. We don't have any form of formal membership, so if you feel like we have some common interests, your participation is welcome!

Would you like to get in touch with us? Contribute to the project? We have different chatrooms you can join. For the moment, all of these chatrooms are in English, although we welcome initiatives to create new ones for other languages:

- {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}}: a general chatroom for the collective, to ask questions and get involved
- {{< chatlink "support" >}}support@joinjabber.org (web chat){{< /chatlink >}}: general Jabber/XMPP user support
- {{< chatlink "servers" >}}servers@joinjabber.org (web chat){{< /chatlink >}}: technical Jabber/XMPP support for server operators
- {{< chatlink "project" >}}project@joinjabber.org (web chat){{< /chatlink >}}: discussions by and with JoinJabber Working Groups
- {{< chatlink "abuse" >}}abuse@joinjabber.org (web chat){{< /chatlink >}}: abuse and moderation in the Jabber/XMPP federation
- {{< chatlink "privacy" >}}privacy@joinjabber.org (web chat){{< /chatlink >}}: privacy and security discussions, for users and server operators
