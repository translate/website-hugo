---
title: "Communauté"
weight: 3
---

Nous sommes un nouveau collective qui souhaite aider à améliorer l'expérience utilisateurice dans l'écosystème Jabber/XMPP. Tu peux découvrir [nos objectifs](@/collective/about/goals/index.fr.md), ou lire [nos derniers comptes-rendus](@/collective/_index.fr.md). Nous n'avons pas d'adhésions formelles, donc si tu sens que nous avons des intérêts en commun, ta participation est bienvenue!

Tu veux rentrer en contact? Participer au projet? Nous avons différentes chatrooms (salons) que tu peux rejoindre. Pour le moment, toutes ces chatrooms sont en Anglais, mais nous accueillons les initiatives pour en créer pour d'autres langues:

- {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}}: discussions générales sur le collectif, et comment participer
- {{< chatlink "support" >}}support@joinjabber.org (web chat){{< /chatlink >}}: support pour les utilisateurices de Jabber/XMPP
- {{< chatlink "servers" >}}servers@joinjabber.org (web chat){{< /chatlink >}}: support technique pour les administrateurs de serveurs Jabber/XMPP
- {{< chatlink "project" >}}project@joinjabber.org (web chat){{< /chatlink >}}: discussions par et avec les groupes de travail de JoinJabber
- {{< chatlink "abuse" >}}abuse@joinjabber.org (web chat){{< /chatlink >}}: abus et modération dans la fédération Jabber/XMPP
- {{< chatlink "privacy" >}}privacy@joinjabber.org (web chat){{< /chatlink >}}: discussions sur la vie privée et la sécurité, pour utilisateurices et admins de serveurs
