---
title: "Objectifs du collectif"
weight: 2
---

Quand nous avons fondé notre collectif, nous nous sommes fixé un certain nombre d'objetifs. Sur cette page, tu trouveras la liste mise-à-jour de ces objectifs, raffinés au cour du temps. Ces objectifs ont été débattus en Anglais pendant nos réunions, et les contributions sont bienvenues.

Désolé, cette page n'a pas encore été traduite ([aide appréciée](https://codeberg.org/joinjabber/website-hugo#helping-with-translations)). Vous pouvez trouver [l'original en anglais ici](/about/goals/).
