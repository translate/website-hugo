---
title: "History"
weight: 6
---

JoinJabber was founded as an informal collective of individuals in early 2021. You can read more about this in our [announcement post on the blog]({{< ref "blog/announcement" >}}).

Here you can find our meeting minutes in reverse-chronological order:

## 2023

- [Meeting minutes from 2023-01-23](meeting03)

## 2022

- Only informal meetings were held with no official meeting minutes

## 2021

- [Meeting minutes from 2021-02-14](meeting02)
- [Meeting minutes from 2021-02-07](meeting01)
