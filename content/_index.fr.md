---
title: "JoinJabber"
draft: true
---

{{< block "grid-2" >}}
{{< column >}}

# Rejoins le réseau Jabber

Une plateforme de réseau social libre et décentralisée

{{< tip >}}
Bienvenue dans la communauté Jabber. Sur cette page, nous t'aiderons à trouver un serveur et un client pour rejoindre le réseau et chatter avec tes amiEs, sans fuiter toutes tes données. Jabber est décentralisé, comme l'email. Cela veut dire que tu dois trouver un serveur, et tu pourras ensuite parler avec tout le monde.
{{< /tip >}}

{{< tip "warning" >}}
Cela peut être un peu compliqué au départ, mais les addresses Jabber (JID) ressemblent aux adresses email. Par exemple, si tu as le compte `louise.michel` sur le serveur jabber.fr, ton adresse sera `louise.michel@jabber.fr`. Les salons de discussions (aussi appelés MUCs) ont des adresses similaires, comme `chat@joinjabber.org`.
{{< /tip >}}

{{< button "docs/" "Allez, c'est parti!" >}}{{< button "https://chat.joinjabber.org/" "Chat" >}}
{{< /column >}}

{{< column >}}
![](/images/undraw_texting.svg)
{{< /column >}}
{{< /block >}}
