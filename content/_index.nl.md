---
title: "JoinJabber"
---

{{< block "grid-2" >}}
{{< column >}}

# Welkom bij de JoinJabber-gemeenschap!

_Een inclusieve plek op het Jabber-netwerk_

{{< tip "warning" >}}
Met Jabber kun je [veilig](docs/faqs/user/#faq-users-encryption) chatten of bellen met je vrienden en familie. Ook kun je deelnemen aan openbare groepschats met mensen die je interesses delen, zonder persoonlijke gegevens prijs te geven.
{{< /tip >}}

{{< tip >}}
Jabber, [ook bekend als **XMPP**](docs/faqs/user/#faq-users-xmppjabber), is een _open standaard_ voor online communicatie. Dit betekent dat het Jabber-netwerk van ons allemaal is, niet van een enkele organisatie.

Jabber is [federatief](docs/faqs/user/#faq-users-federation), net als e-mail of Mastodon. Vele servers zijn met elkaar verbonden om samen het Jabber-netwerk te creëren.
{{< /tip >}}

{{< tip "warning" >}}
Op deze website helpen wij je in twee eenvoudige stappen om je aan te sluiten bij Jabber.

Verder nodigen we je uit je aan te sluiten bij [ons project](about/goals/) om de mogelijkheden van het Jabber-platform verder uit te breiden.
{{< /tip >}}

{{< button "docs/" "Laten we beginnen!" >}}{{< button "https://chat.joinjabber.org/" "Chat met ons" >}}
{{< /column >}}

{{< column >}}
![](/images/undraw_texting.svg)
{{< /column >}}
{{< /block >}}
