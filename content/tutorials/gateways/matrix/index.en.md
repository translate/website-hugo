---
title: "Matrix Gateway"
---

Here, you'll see in the future how to connect to a [Matrix](https://matrix.org) room, with a gateway called called [Bifrost](https://matrix.org/docs/projects/bridge/matrix-bifrost).

## Known public instances

- [aria-net.org](https://aria-net.org/SitePages/Portal/Bridges.aspx) (strongly recommended)
- [matrix.org](https://matrix.org/docs/projects/bridge/matrix-bifrost) (lacking functionality)

## Connection syntax

- Direct chats: `USERNAME_DOMAIN@aria-net.org`
- Public chats: `#ALIAS#DOMAIN@aria-net.org`

## Bridging rooms

It also supports plumbing of existing Matrix rooms to XMPP rooms, Matrix room administrators can perform plumbing by doing the following:

- Invite `@_bifrost_bot:aria-net.org` into the room
- type `!bifrost bridge xmpp-js component.domain.tld roomname`
- To eventually remove the plumbing you can just type `!bifrost leave`

## Connecting from Matrix

Matrix users can also connect to Jabber/XMPP through the same gateway. Connection syntax:

- Private chats: `@_bifrost_USER=40DOMAIN:aria-net.org`
- MUCs: `#_bifrost_MUCNAME_MUCDOMAIN:aria-net.org`



