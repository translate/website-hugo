---
title: "Slidge Gateways"
---

[Slidge](https://slidge.im/) is a work in progress multi-network [puppeteering gateway](/docs/faqs/gateways/#terminology). This means that contrary to other options an account on the legacy network in question is still required. Currently it supports connecting to
[Signal](https://slidge.im/user/plugins/signal.html),
[Telegram](https://slidge.im/user/plugins/telegram.html),
[Discord](https://slidge.im/user/plugins/discord.html),
[Steam Chat](https://slidge.im/user/plugins/steam.html),
[Mattermost](https://slidge.im/user/plugins/mattermost.html),
[Facebook Messenger](https://slidge.im/user/plugins/facebook.html),
[Skype](https://slidge.im/user/plugins/skype.html),
and [WhatsApp](https://slidge.im/user/plugins/whatsapp.html).

This tutorial is a work in progress, please check back later.

### Known public instances

There are currently no known public instances of Slidge. Due to the need to store the legacy-network's user credentials on the server it is unlikely that true public instances will be set up. However, your XMPP server admin (which might be yourself) could offer it as a trusted service.

### Write your own plugin for Slidge

Slidge is designed to be a framework for gateways and can be [easily extended](https://slidge.im/dev/tutorial.html) if a library (ideally in python), a CLI client, or a web API for the legacy network exists.
