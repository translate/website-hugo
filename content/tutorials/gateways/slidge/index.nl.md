---
title: "Slidge Gateways"
---

[Slidge](https://slidge.im/) is een multi-netwerk [puppeteering gateway]({{< ref "docs/faqs/gateways#terminologie" >}}) in ontwikkeling. Dit betekent dat in tegenstelling tot andere opties nog steeds een account op het betreffende netwerk nodig is. Momenteel ondersteunt het verbindingen met
[Signal](https://slidge.im/user/plugins/signal.html),
[Telegram](https://slidge.im/user/plugins/telegram.html),
[Discord](https://slidge.im/user/plugins/discord.html),
[Steam Chat](https://slidge.im/user/plugins/steam.html),
[Mattermost](https://slidge.im/user/plugins/mattermost.html),
[Facebook Messenger](https://slidge.im/user/plugins/facebook.html),
[Skype](https://slidge.im/user/plugins/skype.html),
en [WhatsApp](https://slidge.im/user/plugins/whatsapp.html).

Aan deze handleiding wordt nog gewerkt, kom later nog eens terug.

### Bekende openbare instanties
momenteel geen beknde openbare instanties van Slidge. Omdat de gebruikersgegevens van het legacy-netwerk op de server moeten worden opgeslagen, is het onwaarschijnlijk dat er echte openbare instanties worden opgezet. Je XMPP-serverbeheerder (en dat zou je zelf kunnen zijn) zou het echter kunnen aanbieden als een vertrouwde dienst.

### Schrijf je eigen plug-in voor Slidge

Slidge is ontworpen als een raamwerk voor gateways en kan gemakkelijk [uitgebreid  worden](https://slidge.im/dev/tutorial.html) als er een bibliotheek (idealiter in Python), een CLI-client of een web-API voor het legacy-netwerk bestaat.
