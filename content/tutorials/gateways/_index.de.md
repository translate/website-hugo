---
title: "Gateways"
---

Im Jabber/XMPP Netzwerk, Chatbrücken (Gateways im Englischen) sind eine Möglichkeit mit anderen Chatsystemen über deine Jabber/XMPP App zu komunizieren.

1. **[IRC](/tutorials/gateways/irc/)**
2. **[Matrix](/tutorials/gateways/matrix/)**
3. **[Slidge](/tutorials/gateways/slidge/)**

Du kannst auch bei unserem Gruppenchat für Chatbrücken vorbeischauen: {{< chatlink "bridging" >}}bridging@joinjabber.org (web chat){{< /chatlink >}}

