---
title: "Gateways"
---

In het Jabber/XMPP-ecosysteem zijn "gateways/transports" de middelen om via je client verbinding te maken met andere protocollen.

1. **[IRC](irc/)**
2. **[Matrix](matrix/)**
3. **[Slidge](slidge/)**

U kunt ook deelnemen aan onze gateways en bridges groep chat hier: {{< chatlink "bridging" >}}bridging@joinjabber.org (web chat){{< /chatlink >}}
