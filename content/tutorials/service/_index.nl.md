---
title: "XMPP Services"
---

Hier vind je enkele handleidingen over XMPP-diensten.

1. **[XMPP als openbare dienst](public/)**
2. **[Toegang via Tor](tor/)**
