---
title: "Handleidingen"
weight: 1
---

- **[Gateways](gateways/)**: Maak verbinding met andere netwerken vanuit je Jabber/XMPP-client.
- **[XMPP services](service/)**: Hoe aanvullende openbare diensten te verlenen.
